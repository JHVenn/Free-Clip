# Free Clip

Free Clip is an intuitive multi algorithm soft clipper/wave shaper plugin, available as a Windows VST or mac Audio Unit. The plugin allows you to choose between a range of wave shapes or �sigmoid functions�, from the most transparent but harshest hard clip, to the �softest� but most saturated arctangent shape. You can then intuitively set the ceiling level using the slider that perfectly matches the level meter next to it. Oversampling is also available to remove high frequency aliasing; this does introduce peaks slightly over the ceiling level however, so if you intend to use this plugin as the final plugin on the master chain, ensure the ceiling level is set appropriately and/or the post-oversampling clip setting is set.

This plugin is a great way to conveniently transparently boost volume without clipping your daw, whether it be an individual stem or sound effect, or an entire track that you made. It is recommended that the hardclip, quantic or cubic shape be used for mastering, as these introduce no or minimal saturation.

The plugin can be also used as a more traditional saturation/distortion plugin by setting a �softer� wave shape, such as algebraic or arctangent � simply lower the ceiling level to provide more saturation to the signal � just remember to boost the output afterwards. Alternatively you can boost the input gain into the clipper for the same effect. Increasing the oversampling value is helpful if you�re hearing high frequency aliasing in this case! Be warned that high oversampling values such as 16 or 32 times can be very heavy on the CPU.

### Compiling

- Free Clip uses [Juce](https://www.juce.com/) v4+ framework. You may generate a new JUCE plugin project in Projucer, and replace the source code files with the Free Clip source, remmeber to also add the source direcotry to the includes path in your IDE.
- In this repo exists a "ff-meter" folder, this is a specific version of Foley's Finest "ff_meters" library, which must be added as a JUCE module (not #included) in Projucer.
- If you're building on windows, you will also need the VST3 SDK, which you can download from from the [Steinberg SDK download portal](http://www.steinberg.net/nc/en/company/developers/sdk_download_portal.html).

### Credits
- Free Clip was developed by Jonathan Hyde of [Venn Audio](http://www.vennaudio.com/) and [Small Ocean](http://www.smallocean.net)

##### Thanks to:
- Daniel Walz of Foley's Finest, for the FF-Meters module.
- Vinnie Falco for the DSPFilters library
- Robert Hyde for designing the logo image.

### License
- Free Clip is licensed under the terms of the GNU General Public License Version 3

